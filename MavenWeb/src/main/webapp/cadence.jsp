<%@ page language="java" contentType="text/html; charset=ISO-8859-1"  pageEncoding="ISO-8859-1"%>
<%@page import="java.io.File"%>
<%@page import="java.io.InputStreamReader"%>
<%@page import="java.net.URL"%>
<%@page import="java.io.FileReader"%>
<%@page import="java.io.BufferedReader"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<style>
		body{background: #F1F0D1; font-family: verdana, Tahoma, Arial, Sans-Sarif; font-size: 18px; overflow: auto;}
		h1,h2,h3 {text-align:center;padding-left:5%;color:#878E63;}
		p{padding:2%; color:#878E63;}
		#wrapper{margin:0 auto; max-width:100%; width:98%;background:#FEFBE8; border:1px solid  #878E63;border-radius:2px;box-shadow: 0 0 10px 0px rgba(12,3,25,0.8);}
		#callout{width: 100%;  height:auto; background:#878E63;overflow:hidden;}
		#callout p{text-align:right; font-size:13px;padding:0.1% 5% 0 0 ;color:#F1F0D1}
		header{width:96%; min-height:25px; padding:1px; text-align:center;}
		nav ul{list-style:none; margin:0;padding-left:50px;}
		nav ul li{float:left; border:1px solid #878E63; width:15%;}
		nav ul li a {background: #F1F0D1; display:block; padding: 5% 12%;font-weight:bold; font-size: 18px; color: #878E63; text-decoration: none; text-align:center;}
		nav ul li a:hover, nav ul li.active a {background-color:#878E63; color: #F1F0D1;}
		.left-col {width: 55%; float:left; margin: -2% 1% 1% 1%;}
		.sidebar {width:40%; float:right; margin:1%; text-align:center;}
		.section{width:33%; float:left; margin 2% 2%; text-align:center;}
		.banner {width:100%; border-top: 0px solid #878E63; border-bottom:0px solid #878E63;}
		footer{background: #878E63; width:100%; overflow: hidden;height:70px;}
		footer p, footer h3{color:#F1F0D1;}
		footer p a{color:#F1F0D1; text-decoration:none;}
		ul{list-style:none;margin:0; padding:0;}
		li{display:inline;}
		@media screen and (max-width: 478px){body{font-size:13px;}}
		@media screen and (max-width:740px){
			nav{width:100%; margin-bottom:10px;}
			nav ul{list-style:none;margin:0 auto; padding-left:0;}
			nav ul li{text-align:center; margin-left:0 auto; width:100%; border-top:1px solid #878E63; border-right:1px solid #878E63; border-bottom:1px solid #878E63; border-left:1px solid #878E63;}
			nav ul li a{padding:8px 0; font-size:16px;}
			.left-col{width:100%;}
			.sidebar{width:100%;}
			.section{float:right; width:100%;margin:0;}
		}
		.google-visualization-table-td {text-align: center !important;}
		a.paginate_button {display: inline-block; font-size:12px;}
		div.dataTables_info, div.dataTables_length {display: inline-block; font-size:12px; padding-left:5.5em;}
		div.dataTables_filter,div.dataTables_paginate {display: inline-block; font-size:12px;padding-right:5.5em;}
		th{background-color:#878E63;}
		#cadenceTable{border: 1px solid black !important;}
</style>

		
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Focus Quality Dashboard</title>
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.12/css/jquery.dataTables.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.2/jquery.min.js"></script>
<script src="http://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>


<script>
$(document).ready(function(){
    $('#cadenceTable').DataTable({"language": {"lengthMenu": "Displaying _MENU_ Deliverables and Production Defects","info": "Page _PAGE_ of _PAGES_","sSearch": "Search for the tickets:","zeroRecords": "No tickets matching the search criteria! Please retry."}});
    $('#cadenceTable').css('font-size', "0.8em");   
    //$('.dataTables_filter').each(function () {$(this).append('&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;');});
});
</script>
</head>
<body onload="selectedValue()">
<div id="wrapper">
<div id="callout"><p></p></div><br/>
		<nav>
			<ul>
				<li><a href="home.jsp">Home</a></li>
				<li class="active"><a href="cadence.jsp">Cadence</a></li>
				<li><a href="release.jsp">Release</a></li>
				<li><a href="project.jsp">Team</a></li>
				<li><a href="kanban.jsp">Kanban</a></li>
				<li><a href="#">Settings</a></li>
			</ul>
		</nav>
		<center><br/><br/>
		<hr>
		<form action="MyServ" id="f1" name="f1">
			Cadence:
<!-- 			<select name="cadence" onchange="fillBox()" id="cadence" >
				<option>--Select Cadence---</option>
				<option>16prod01</option><option>16prod02</option><option>16prod03</option>
				<option>16prod04</option><option>16prod05</option><option>16prod06</option>
		    </select> -->
		
		<%
            //String jspPath = session.getServletContext().getRealPath("/res");
            String txtFilePath = application.getRealPath("/") + "Config.txt";
            BufferedReader reader = new BufferedReader(new FileReader(txtFilePath));
            StringBuilder sb = new StringBuilder();
            String line;
			sb.append("<select name=" + "\"" + "cadence" + "\"" + " onchange=" + "\"" + "fillBox()" + "\"" + " id=" + "\"" + "cadence" + "\"" + ">");
            while((line = reader.readLine())!= null){
            	if(line.substring(0,7).equals("Cadence")){          		
            		line=line.replace("Cadence:","");
            		sb.append("<option>"+line+"</option>");}}
            sb.append("</select>");
            out.println(sb.toString());
        %>
			<input type="text" id="txtQuery" style="width: 1px; " name="txtQuery" />
			<!-- <input type="text" id="txtQuery" style="width: 480px; " name="txtQuery" onkeypress="return searchKeyPress(event)" /> -->
			<input type="hidden" name="pname" value= "cadence">
			<input type="hidden" name="selectedCadence" value="0"/> 
			<input type="submit" value="" id="send" />
		</form>	
		
		<table>
<tr>
		<td width="300px" height="300px" id="p1"></td>
		<td></td><td></td><td></td><td></td><td></td><td></td><td></td>
		<td width="300px" height="300px" id="p2"></td>
	</tr>
	<tr>
		<td width="100px" height="40px" id="t1"></td>
		<td></td><td></td><td></td><td></td><td></td><td></td><td></td>
		<td width="100px" height="40px" id="t2"></td>
	</tr>
</table>
		<br>
		<%if(request.getAttribute("cadenceTable") == null){request.setAttribute("cadenceTable","");} %>
		<%=request.getAttribute("cadenceTable")%> 
		<table><tr><td id="t3"></td></tr></table>
		</center>
		<br>
 		<div id="gauge_div" style="width:900px; height: 360px;"></div> 
		
		<% String tblTypeCol = (String) session.getAttribute("tblTypeCol"); %>
		<% String tblTypeRow = (String) session.getAttribute("tblTypeRow"); %>
		<% String tblStatusCol = (String) session.getAttribute("tblStatusCol"); %>
		<% String tblStatusRow = (String) session.getAttribute("tblStatusRow"); %>
		<% String tblDetailRow = (String) session.getAttribute("detailtablerow"); %>
		<% String tblDetailCol = (String) session.getAttribute("detailtablecol"); %>
		<% String pieType = (String) session.getAttribute("pieType"); %>
		<% String pieStatus = (String) session.getAttribute("pieStatus"); %>
		<%if(request.getAttribute("Name") == null){request.setAttribute("Name","");} %>
		<%-- <%=request.getAttribute("Name")%> --%>
<footer>
	<div class="section"><p><a href="mailto:zakir.syed@teamfocusins.com?Subject=Feedback%20on%20Quality%20Dashboard" target="_top">Email us</a></p></div>
	<div class="section"><p>� 2016 Product Quality Team</p></div>
	<div class="section"><p><a href="http://teamfocusins.com/">Focus Tech</a><a href="https://www.cognizant.com/"> and Cognizant</a></p></div>
</footer>
	</div>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script type="text/javascript">
    google.charts.load('current', {'packages':['gauge','table','corechart']});
    google.charts.setOnLoadCallback(drawChart);
    var gaugeOptions = {min: 0, max: 280, yellowFrom: 200, yellowTo: 250,redFrom: 250, redTo: 280, minorTicks: 5};
    var gauge;
    function drawChart(){
    	drawTypePie();
    	drawStatusPie();
    	drawTypeTable();
    	drawStatusTable();
    	//drawDetailTable();
    	checkTable();
    }
    function drawTypePie() {
    	var pieType = <%=pieType%>;
    	pieData = new google.visualization.arrayToDataTable(pieType);
    	var options = {titlePosition: 'none',pieHole: 0.3, backgroundColor:'#FEFBE8', legend:{position:'bottom'}};
      	var pie = new google.visualization.PieChart(document.getElementById('p1'));
        pie.draw(pieData, options);
    }
    
    function drawStatusPie() {
    	var pieStatus = <%=pieStatus%>;
     	StData = new google.visualization.arrayToDataTable(pieStatus);
    	var opt = {titlePosition: 'none',pieHole: 0.3, backgroundColor:'#FEFBE8', legend:{position:'bottom'}};
      	var stat = new google.visualization.PieChart(document.getElementById('p2'));
      	stat.draw(StData, opt);
    }
    
    function drawTypeTable() {
    	var tblTypeRow = <%=tblTypeRow%>;
        var tblTypeCol = <%=tblTypeCol%>;
        var numRows = tblTypeRow.length;
        var numCols = tblTypeRow[0].length;    	
    	var tblType = new google.visualization.DataTable();
        tblType.addColumn('string', tblTypeCol[0]);
        for (var i = 1; i < numCols; i++)
        	tblType.addColumn('number', tblTypeCol[i]); 
        for (var i = 0; i < numRows; i++)
        	tblType.addRow(tblTypeRow[i]);            
        var table = new google.visualization.Table(document.getElementById('t1'));
        table.draw(tblType, {showRowNumber: false, width: '100%', height: '100%'});
    }
    
    function drawStatusTable() {
    	var tblStatusRow = <%=tblStatusRow%>;
        var tblStatusCol = <%=tblStatusCol%>;
        var cntRow = tblStatusRow.length;
        var cntCol = tblStatusRow[0].length;
    	var tblStatus = new google.visualization.DataTable();
        tblStatus.addColumn('string', tblStatusCol[0]);
        for (var i = 1; i < cntCol; i++)
        	tblStatus.addColumn('number', tblStatusCol[i]); 
        for (var i = 0; i < cntRow; i++)
        	tblStatus.addRow(tblStatusRow[i]);            
        var table1 = new google.visualization.Table(document.getElementById('t2'));
        table1.draw(tblStatus, {showRowNumber: false, width: '100%', height: '100%'});
    }
    
    function drawDetailTable() {
    	var tblDetailRow = <%=tblDetailRow%>;
        var tblDetailCol = <%=tblDetailCol%>;
        var detRow = tblDetailRow.length;
        var detCol = tblDetailRow[0].length;
    	var tblDetail = new google.visualization.DataTable();
        tblDetail.addColumn('string', tblDetailCol[0]);        
        for (var i = 1; i < detCol; i++)
        	tblDetail.addColumn('string', tblDetailCol[i]); 
        for (var i = 0; i < detRow; i++)
        	tblDetail.addRow(tblDetailRow[i]);                    
        var table2 = new google.visualization.Table(document.getElementById('t3'));
        function selectHandler() { var selectedItem = table2.getSelection()[0];
        	if (selectedItem) { var topping = tblDetail.getValue(selectedItem.row, 2);	
            var temp='https://focustech.atlassian.net/browse/' + topping; window.open(temp,'_blank');}}     
        google.visualization.events.addListener(table2, 'select', selectHandler);    	  
        table2.draw(tblDetail, {showRowNumber: true, width: '100%', height: '100%'});
   }
    
   function fillBox()
    {
    	document.getElementById("txtQuery").value="cadence in(" + document.getElementById("cadence").value + "," + document.getElementById("cadence").value + "-1) and project !=SDLC and type in (deliverable, " + "\"" + "Production Defect" + "\"" + ") and summary!~" + "\"" + "general support" + "\"" ;
    	document.f1.selectedCadence.value=document.f1.cadence.selectedIndex;
    	f1.submit();
    }
    function selectedValue(){  
     	var value =<%=request.getParameter("selectedCadence")%>;  
        if(value !=null){document.f1.cadence.selectedIndex=value; document.getElementById("t1").style.display='inline';document.getElementById("t2").style.display='inline';document.getElementById("t3").style.display='inline';document.getElementById("p1").style.display='table-cell';document.getElementById("p2").style.display='table-cell';}
        else{document.getElementById("t1").style.display='none';document.getElementById("t2").style.display='none';document.getElementById("t3").style.display='none';document.getElementById("p1").style.display='none';document.getElementById("p2").style.display='none';}
        if (document.getElementById("txtQuery").value==""){document.getElementById("txtQuery").value="cadence in(" + document.getElementById("cadence").value + "," + document.getElementById("cadence").value + "-1) and project !=SDLC and type in (deliverable, " + "\"" + "Production Defect" + "\"" + ") and summary!~" + "\"" + "general support" + "\"" ;}
        document.getElementById("txtQuery").style.display='none';
        document.getElementById("send").style.display='none';
    }  
    function checkTable(){
    	if(document.getElementById("t3").style.display=='inline'){document.getElementById("gauge_div").style.display='none'};
    } 
    
</script>
</body>
</html>

