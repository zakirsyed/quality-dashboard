<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<style>
		body{background: #F1F0D1; font-family: verdana, Tahoma, Arial, Sans-Sarif; font-size: 18px; overflow: auto;}
		h1,h2,h3 {text-align:center;padding-left:5%;color:#878E63;}
		p{padding:2%; color:#878E63;}
		#wrapper{margin:0 auto; max-width:100%; width:98%;background:#FEFBE8; border:1px solid  #878E63;border-radius:2px;box-shadow: 0 0 10px 0px rgba(12,3,25,0.8);}
		#callout{width: 100%;  height:auto; background:#878E63;overflow:hidden;}
		#callout p{text-align:right; font-size:13px;padding:0.1% 5% 0 0 ;color:#F1F0D1}
		header{width:96%; min-height:5px; padding:0px; text-align:center;}
		nav ul{list-style:none; margin:0;padding-left:50px;}
		nav ul li{float:left; border:1px solid #878E63; width:15%;}
		nav ul li a {background: #F1F0D1; display:block; padding: 5% 12%;font-weight:bold; font-size: 18px; color: #878E63; text-decoration: none; text-align:center;}
		nav ul li a:hover, nav ul li.active a {background-color:#878E63; color: #F1F0D1;}
		.left-col {width: 55%; float:left; margin: -2% 1% 1% 1%;}
		.sidebar {width:40%; float:right; margin:1%; text-align:center;}
		.section{width:33%; float:left; margin 2% 2%; text-align:center;}
		.banner {width:100%; border-top: 0px solid #878E63; border-bottom:0px solid #878E63;}
		footer{background: #878E63; width:100%; overflow: hidden;height:70px;}
		footer p, footer h3{color:#F1F0D1;}
		footer p a{color:#F1F0D1; text-decoration:none;}
		ul{list-style:none;margin:0; padding:0;}
		li{display:inline;}
		@media screen and (max-width: 478px){body{font-size:13px;}}
		@media screen and (max-width:740px){
		nav{width:100%; margin-bottom:2px; margin-top:10px;}
		nav ul{list-style:none;margin:0 auto; padding-left:0;}
		nav ul li{text-align:center; margin-left:0 auto; width:100%; border-top:1px solid #878E63; border-right:1px solid #878E63; border-bottom:1px solid #878E63; border-left:1px solid #878E63;}
		nav ul li a{padding:8px 0; font-size:16px;}
		.left-col{width:100%;}
		.sidebar{width:100%;}
		.section{float:right; width:10%;margin:#FF0000;}
		}
		* {text-rendering: optimizeLegibility; font-size:100%;}
		svg g g {font-size:20px;}
		svg g{font-size:18px;}
		.google-visualization-table-td {text-align: center !important;}
		</style>	
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Focus Quality Dashboard</title>
</head>
<body onload="selectedValue()">
<div id="wrapper">
<div id="callout"><p></p></div><br/>
<nav>
			<ul>
				<li class="active"><a href="home.jsp">Home</a></li>
				<li><a href="cadence.jsp">Cadence</a></li>
				<li><a href="release.jsp">Release</a></li>
				<li><a href="project.jsp">Team</a></li>
				<li><a href="kanban.jsp">Kanban</a></li>
				<li><a href="#">Settings</a></li>
			</ul>
</nav><br/><br/>
<center>
<hr>
<%if(session.getAttribute("userName") == ""){session.setAttribute("userName","Guest");} %>
<%if(session.getAttribute("ticketCtr") == ""){session.setAttribute("ticketCtr","No");} %>
<div align="center">Welcome, <%=session.getAttribute("userName")%>! You have <a href="https://focustech.atlassian.net/issues/?jql=assignee%20%3D%20currentUser()%20and%20status%20!%3D%20closed"  target="_blank"><%=session.getAttribute("ticketCtr")%></a> outstanding ticket(s) assigned to you!</div>
<div>
<br>
<table>
<tr>
		<td width="300px" height="300px" id="g1"></td>
		<td></td><td></td><td></td><td></td><td></td><td></td><td></td>
		<td width="300px" height="300px" id="g2"></td>
		<td></td><td></td><td></td><td></td><td></td><td></td><td></td>
		<td width="300px" height="300px" id="g3"></td>
	</tr>
	<tr>
		<td width="100px" height="45px" id="t1"></td>
		<td></td><td></td><td></td><td></td><td></td><td></td><td></td>
		<td width="100px" height="45px" id="t2"></td>
		<td></td><td></td><td></td><td></td><td></td><td></td><td></td>
		<td width="100px" height="45px" id="t3"></td>
	</tr>
</table>
</div>
<form action="MyServ" id="f1" name="f1" >
			<input type="hidden" name="pname" value= "release">
			<input type="hidden" name="selectedValue" value="0"/>  
</form>
</center>
<br><br>
		<% String cadence = (String) session.getAttribute("cadence"); %>
		<% String release = (String) session.getAttribute("release"); %>
		<% String defect = (String) session.getAttribute("defect"); %>
		<% String cadencetabledata = (String) session.getAttribute("cadencetabledata"); %>
		<% String releasetabledata = (String) session.getAttribute("releasetabledata"); %>
		<% String defectstabledata = (String) session.getAttribute("defectstabledata"); %>
		<% String cadencecoldata = (String) session.getAttribute("cadencecoldata"); %>
		<% String releasecoldata = (String) session.getAttribute("releasecoldata"); %>
		<% String defectscoldata = (String) session.getAttribute("defectscoldata"); %>
		
<footer>
	<div class="section"><p><a href="mailto:zakir.syed@teamfocusins.com?Subject=Feedback%20on%20Quality%20Dashboard" target="_top">Email us</a></p></div>
	<div class="section"><p>� 2016 Product Quality Team</p></div>
	<div class="section"><p><a href="http://teamfocusins.com/">Focus Tech</a><a href="https://www.cognizant.com/"> and Cognizant</a></p></div>
</footer>
</div>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script type="text/javascript">
	var cadence=<%=cadence%>;
	var release=<%=release%>;
	var defect=<%=defect%>;

	google.charts.load('current', {'packages':['gauge','table']});
    google.charts.setOnLoadCallback(drawChart);
    var gaugeOptions = {min: 0, max: 100, yellowFrom: 75, yellowTo: 95,redFrom: 0, redTo: 75, minorTicks: 5, greenFrom:95, greenTo:100};
    var gauge;
    function drawChart(){
    	drawCadenceGauge();
    	drawReleaseGauge();
    	drawDefectsGauge();
		drawCadenceTable();
		drawReleaseTable();
		drawDefectsTable();
    }
    function drawCadenceGauge() {
      gaugeData = new google.visualization.DataTable();
      gaugeData.addColumn('number', 'Cadence');
      gaugeData.addRows(1);
      gaugeData.setCell(0, 0, cadence);
      var formatter = new google.visualization.NumberFormat({suffix: '%',pattern:'#'});
      formatter.format(gaugeData,0);
      gauge = new google.visualization.Gauge(document.getElementById('g1'));
      gauge.draw(gaugeData, gaugeOptions);
      
    }
    function drawReleaseGauge() {
        gaugeData = new google.visualization.DataTable();
        gaugeData.addColumn('number', 'Release');
        gaugeData.addRows(1);
        gaugeData.setCell(0, 0, release);
        var formatter = new google.visualization.NumberFormat({suffix: '%',pattern:'#'});
        formatter.format(gaugeData,0);
        gauge = new google.visualization.Gauge(document.getElementById('g2'));
        gauge.draw(gaugeData, gaugeOptions);
      }
    function drawDefectsGauge() {
        gaugeData = new google.visualization.DataTable();
        gaugeData.addColumn('number', 'Defect Closure');
        gaugeData.addRows(1);
        gaugeData.setCell(0, 0, defect);
        var formatter = new google.visualization.NumberFormat({suffix: '%',pattern:'#'});
        formatter.format(gaugeData,0);
        gauge = new google.visualization.Gauge(document.getElementById('g3'));
        gauge.draw(gaugeData, gaugeOptions);
      }
   /*  function drawCadenceGauge() {
        gaugeData = new google.visualization.DataTable();
        gaugeData.addColumn('number', 'Cadence');
        gaugeData.addColumn('number', 'Release');
        gaugeData.addColumn('number', 'Defect Closure');
        gaugeData.addRows(3);
        gaugeData.setCell(0, 0, cadence);
        gaugeData.setCell(0, 1, release);
        gaugeData.setCell(0, 2, defect);
        gauge = new google.visualization.Gauge(document.getElementById('g1'));
        gauge.draw(gaugeData, gaugeOptions);
      } */
    
    var cadenceData = <%=cadencetabledata%>;
    var colData = <%=cadencecoldata%>;
    var numRows = cadenceData.length;
    var numCols = cadenceData[0].length;
    function drawCadenceTable() {
        var dataTable = new google.visualization.DataTable();
        dataTable.addColumn('string', colData[0]);
        for (var i = 1; i < numCols; i++)
            dataTable.addColumn('number', colData[i]); 
        for (var i = 0; i < numRows; i++)
            dataTable.addRow(cadenceData[i]);            
        var table = new google.visualization.Table(document.getElementById('t1'));
        table.draw(dataTable, {showRowNumber: false, width: '100%', height: '100%'});
    }
    var releaseData = <%=releasetabledata%>;
    var releasecolData = <%=releasecoldata%>;
    var relRows = releaseData.length;
    var relCols = releaseData[0].length;
    function drawReleaseTable() {
        var dataTable = new google.visualization.DataTable();
        dataTable.addColumn('string', releasecolData[0]);
        for (var i = 1; i < relCols; i++)
            dataTable.addColumn('number', releasecolData[i]); 
        for (var i = 0; i < relRows; i++)
            dataTable.addRow(releaseData[i]);            
        var table = new google.visualization.Table(document.getElementById('t2'));
        table.draw(dataTable, {showRowNumber: false, width: '100%', height: '100%'});
    }
    var defectsData = <%=defectstabledata%>;
    var defectscolData = <%=defectscoldata%>;
    var defRows = defectsData.length;
    var defCols = defectsData[0].length;
    function drawDefectsTable() {
        var dataTable = new google.visualization.DataTable();
        dataTable.addColumn('string', defectscolData[0]);
        for (var i = 1; i < defCols; i++)
            dataTable.addColumn('number', defectscolData[i]); 
        for (var i = 0; i < defRows; i++)
            dataTable.addRow(defectsData[i]);            
        var table = new google.visualization.Table(document.getElementById('t3'));
        table.draw(dataTable, {showRowNumber: false, width: '100%', height: '100%'});
    }
    function fillBox()
    {
    	document.getElementById("txtQuery").value="fixVersion=" + document.getElementById("release").value + " and project=SDLC and type in (epic, " + "\"" + "Production Defect" + "\"" + ") and summary!~" + "\"" + "general support" + "\"" ;
    	document.f1.selectedValue.value=document.f1.release.selectedIndex;
    	f1.submit();
    }
    function selectedValue(){  
    	/* var value =null;  
        if(value !=null){document.f1.release.selectedIndex=value; }
        if (document.getElementById("txtQuery").value==""){document.getElementById("txtQuery").value="fixVersion=" + document.getElementById("release").value + " and project=SDLC and type in (epic, " + "\"" + "Production Defect" + "\"" + ") and summary!~" + "\"" + "general support" + "\"" ;}
        document.getElementById("txtQuery").style.display='none';
        document.getElementById("send").style.display='none'; 
        f1.submit();*/
    }   
</script>
</body>
</html>

