package zak;
import java.io.IOException;
import java.io.StringReader;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.XML;
import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.core.util.Base64;

public class getUS {
	public String str;		public String elementName; 		public int don;    	public int wad;			public String htmlTable;    		
	public int can;			public int unr;					public int wit;     public int rep;			public int dup;
	
	public void getUserStories(String userName,String password,String parentKeys){
		don=0; wad=0; can=0; wit=0; rep=0; unr=0; dup=0;str="["; 
		htmlTable="<table id=" + "\"" + "userStory" + "\"" + "class=" + "\"" + "display compact" + "\"" + " cellspacing=" + "\"" + "0" + "\"" + " width=" + "\"" + "90%" + "\"" + "><thead><tr><th>Summary</th><th>Status</th><th>Key</th></tr></thead>";
        try {	Client client=Client.create();
				String auth = new String(Base64.encode(userName + ":" + password));
				parentKeys=parentKeys.replace("=","%3D");		parentKeys=parentKeys.replace(" ","%20");			parentKeys=parentKeys.replace("\"","%22");
				parentKeys=parentKeys.replace("done,", "");		parentKeys=parentKeys.replace("indeterminate,", "");	parentKeys=parentKeys.replace("new,", "");
				parentKeys=parentKeys + "&fields=key,status,summary&maxResults=10000";
				//System.out.println(parentKeys);
				WebResource resource=client.resource("https://focustech.atlassian.net/rest/api/2/search?jql=" + parentKeys);
				ClientResponse resp=resource.header("Authorization", "Basic "+ auth).type("application/json").accept("application/json").get(ClientResponse.class);
				JSONObject json=new JSONObject(resp.getEntity(String.class));
				String xml=XML.toString(json);
				//System.out.println(xml);
				xml="<root>" + xml + "</root>";	xml=xml.replace("&", "");
				xml=xml.replace("<48x48>","");	xml=xml.replace("</48x48>","");	xml=xml.replace("<24x24>","");	xml=xml.replace("</24x24>","");
				xml=xml.replace("<32x32>","");	xml=xml.replace("</32x32>","");	xml=xml.replace("<16x16>","");	xml=xml.replace("</16x16>","");
				xml=xml.replaceAll("<key>new</key>", "");
				xml = xml.replaceAll("\\[", "").replaceAll("\\]","");	
				if(xml.equalsIgnoreCase("<root><total>0</total><maxResults>1000</maxResults><startAt>0</startAt></root>")){str="[['1','1','1']]";}
				else{
				SAXParser p=SAXParserFactory.newInstance().newSAXParser();
				DefaultHandler handler=new DefaultHandler(){
					public void startDocument() throws SAXException{}
					public void startElement(String uri, String localName,String qName,Attributes attributes) throws SAXException{elementName=qName.trim();}
					public void characters(char ch[], int start, int length) throws SAXException{
						String tempStr=new String(ch,start,length).trim();
						//System.out.println(tempStr);
						tempStr = tempStr.replace("(", "").replace(")", "");
						tempStr=tempStr.replace("\"", "");		tempStr=tempStr.replace("'", "");		tempStr=tempStr.replace("indeterminate", "").replace("done", "");
						tempStr=tempStr.replace("*", "");		tempStr=tempStr.replace("=", "");		tempStr=tempStr.replace("#", "");
						if (tempStr.length()>0){tempStr= tempStr.substring(0, Math.min(tempStr.length(), 80));}
						switch(elementName){
							case "key":
								if(tempStr.length()>0){tempStr="<a href="+ "\"" + "https://focustech.atlassian.net/browse/" + tempStr + "\"" + "target=" + "\"" + "blank" + "\"" + ">" + tempStr + "</a>";
									str=str + "\'" + tempStr + "'],";htmlTable=htmlTable+"<td>" + tempStr.trim() + "</td></tr>";}
								break;
							case "summary":
								if(tempStr.length()>0){str=str + "['" + tempStr.trim() + "',";htmlTable=htmlTable+"<tr><td>" + tempStr.trim() + "</td>";}
								break;
							case "name" :
								if (tempStr.equalsIgnoreCase("Closed"))			{str=str + "\'" +  tempStr + "',";htmlTable=htmlTable+"<td>" + tempStr.trim() + "</td>";}
								if (tempStr.equalsIgnoreCase("BA"))				{str=str + "\'" +  tempStr + "',";htmlTable=htmlTable+"<td>" + tempStr.trim() + "</td>";}
								if (tempStr.equalsIgnoreCase("Staging"))		{str=str + "\'" +  tempStr + "',";htmlTable=htmlTable+"<td>" + tempStr.trim() + "</td>";}
								if (tempStr.equalsIgnoreCase("Ready Queue"))	{str=str + "\'" +  tempStr + "',";htmlTable=htmlTable+"<td>" + tempStr.trim() + "</td>";}
								if (tempStr.equalsIgnoreCase("BA Done"))		{str=str + "\'" +  tempStr + "',";htmlTable=htmlTable+"<td>" + tempStr.trim() + "</td>";}
								if (tempStr.equalsIgnoreCase("Dev In Progress")){str=str + "\'" +  tempStr + "',";htmlTable=htmlTable+"<td>" + tempStr.trim() + "</td>";}
								if (tempStr.equalsIgnoreCase("Dev Done"))		{str=str + "\'" +  tempStr + "',";htmlTable=htmlTable+"<td>" + tempStr.trim() + "</td>";}
								if (tempStr.equalsIgnoreCase("QA Done"))		{str=str + "\'" +  tempStr + "',";htmlTable=htmlTable+"<td>" + tempStr.trim() + "</td>";}
								if (tempStr.equalsIgnoreCase("QA In Progress"))	{str=str + "\'" +  tempStr + "',";htmlTable=htmlTable+"<td>" + tempStr.trim() + "</td>";}
								if (tempStr.equalsIgnoreCase("UAT In Progress")){str=str + "\'" +  tempStr + "',";htmlTable=htmlTable+"<td>" + tempStr.trim() + "</td>";}
								if (tempStr.equalsIgnoreCase("UAT Deployed")){str=str + "\'" +  tempStr + "',";htmlTable=htmlTable+"<td>" + tempStr.trim() + "</td>";}
								if (tempStr.equalsIgnoreCase("UAT Done"))		{str=str + "\'" +  tempStr + "',";htmlTable=htmlTable+"<td>" + tempStr.trim() + "</td>";}
								if (tempStr.equalsIgnoreCase("PROD"))			{str=str + "\'" +  tempStr + "',";htmlTable=htmlTable+"<td>" + tempStr.trim() + "</td>";}
								break;
						}}
					public void endElement(String uri, String localName,String qName) throws SAXException{}
					public void endDocument() throws SAXException{
						if (str.length()>0){str=str.substring(0, Math.min(str.length(), str.length()-1)); str=str + "]";}
						htmlTable=htmlTable + "</table>";
						}};
				p.parse(new InputSource(new StringReader(xml)),handler);
				//System.out.println(str);
				//System.out.println(htmlTable);
				}}
		catch (IOException e) {e.printStackTrace();}	
		catch (JSONException e) {e.printStackTrace();}		
 		catch (ParserConfigurationException e) {e.printStackTrace();}
		catch (SAXException e) {e.printStackTrace();}}
}