package zak;

import java.io.IOException;
import java.io.StringReader;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.XML;
import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.core.util.Base64;

public class GetCadence {
	public String str;	public String elementName;  public String tempStr;	public int uat;		public int exec;			public int hl;
    public int ba;		public int ready;			public int don;			public String htmlTable;
       
    public void getCadence(String userName,String password,String parentKeys){
        try {	
        	
        	htmlTable="<table id=" + "\"" + "cadenceTable" + "\"" + "class=" + "\"" + "display compact" + "\"" + " cellspacing=" + "\"" + "0" + "\"" + " width=" + "\"" + "90%" + "\"" + "><thead><tr><th>Summary</th><th>Resolution</th><th>Key</th></tr></thead>";            
        		uat=0; exec=0; hl=0; ba=0; ready=0; don=0; str="[";
        		Client client=Client.create();
				String auth = new String(Base64.encode(userName + ":" + password));
				parentKeys=parentKeys.replace("=","%3D");		parentKeys=parentKeys.replace(" ","%20");			parentKeys=parentKeys.replace("\"","%22");
				parentKeys=parentKeys.replace("done,", "");		parentKeys=parentKeys.replace("indeterminate,", "");
				parentKeys=parentKeys + "&fields=key,status,summary&maxResults=10000";
				//System.out.println(parentKeys);
				WebResource resource=client.resource("https://focustech.atlassian.net/rest/api/2/search?jql=" + parentKeys);
				ClientResponse resp=resource.header("Authorization", "Basic "+ auth).type("application/json").accept("application/json").get(ClientResponse.class);
				JSONObject json=new JSONObject(resp.getEntity(String.class));
				String xml=XML.toString(json);
				//System.out.println(xml);
				xml="<root>" + xml + "</root>";	xml=xml.replaceAll("&", "");
				xml=xml.replace("<48x48>","");	xml=xml.replace("</48x48>","");	xml=xml.replace("<24x24>","");	xml=xml.replace("</24x24>","");
				xml=xml.replace("<32x32>","");	xml=xml.replace("</32x32>","");	xml=xml.replace("<16x16>","");	xml=xml.replace("</16x16>","");
				SAXParser p=SAXParserFactory.newInstance().newSAXParser();
				DefaultHandler handler=new DefaultHandler(){		
					public void startDocument() throws SAXException{
						//str="<table style=" + "\"" + "width:100%" + "\"" + "border=" + "\"" + "1" + "\"" + ">";
						//str=str + "<tr><td>Summary</td><td>Status</td><td>Ticket</td><tr>";
						}
					public void startElement(String uri, String localName,String qName,Attributes attributes) throws SAXException{elementName=qName.trim();}
					public void characters(char ch[], int start, int length) throws SAXException{
						tempStr=new String(ch,start,length).trim();
						tempStr= tempStr.substring(0, Math.min(tempStr.length(), 80));
						tempStr=tempStr.replace("'", "");
						if (elementName.equalsIgnoreCase("key")){
							if(tempStr.equalsIgnoreCase("done") || tempStr.equalsIgnoreCase("indeterminate") || tempStr.equalsIgnoreCase("new")){}
							else{//tempStr="<a href=" + "\"" + "https://focustech.atlassian.net/browse/" + tempStr + "\"" + ">" + tempStr + "</a>";
							//str=str + "<td>" + tempStr + "</td>";str=str + "</tr>";
							str=str + "\'" + tempStr + "'],";
							tempStr="<a href="+ "\"" + "https://focustech.atlassian.net/browse/" + tempStr + "\"" + "target=" + "\"" + "blank" + "\"" + ">" + tempStr + "</a>";
							htmlTable=htmlTable+"<td>" + tempStr.trim() + "</td></tr>";
							}
							if(tempStr.equalsIgnoreCase("new")){str=str + "\'" +  tempStr + "',";htmlTable=htmlTable+"<td>" + tempStr.trim() + "</td>";}
						}
						if (elementName.equalsIgnoreCase("summary")){str=str + "['" + tempStr + "',";htmlTable=htmlTable+"<tr><td>" + tempStr.trim() + "</td>";}
						if (elementName.equalsIgnoreCase("name")){
							switch (tempStr){
							case "Closed": don=don+1; str=str + "\'" +  tempStr + "',"; htmlTable=htmlTable+"<td>" + tempStr.trim() + "</td>";break;				
							case "Delivered": don=don+1; str=str + "\'"  + tempStr + "',"; htmlTable=htmlTable+"<td>" + tempStr.trim() + "</td>";break;
							case "In UAT": uat=uat+1; str=str + "\'" + tempStr + "',"; htmlTable=htmlTable+"<td>" + tempStr.trim() + "</td>";break;				
							case "UAT Done": uat=uat+1; str=str + "\'" + tempStr + "',"; htmlTable=htmlTable+"<td>" + tempStr.trim() + "</td>";break;	
							case "In Execution": exec=exec+1; str=str + "\'" + tempStr + "',"; htmlTable=htmlTable+"<td>" + tempStr.trim() + "</td>";break; 		
							case "On Deck" : exec=exec+1; str=str + "\'" + tempStr + "',"; htmlTable=htmlTable+"<td>" + tempStr.trim() + "</td>";break;	
							case "Pending HL Estimate": hl=hl+1; str=str + "\'" + tempStr + "',"; htmlTable=htmlTable+"<td>" + tempStr.trim() + "</td>";break; 	
							case "HL Estimate Complete" : hl=hl+1; str=str + "\'" + tempStr + "',"; htmlTable=htmlTable+"<td>" + tempStr.trim() + "</td>";break;	
							case "In HL Estimate" : hl=hl+1; str=str + "\'" + tempStr + "',"; htmlTable=htmlTable+"<td>" + tempStr.trim() + "</td>";break;		
							default: ready=ready+1; break;}}}
					public void endElement(String uri, String localName,String qName) throws SAXException{}
					public void endDocument() throws SAXException{}};
					p.parse(new InputSource(new StringReader(xml)),handler);
					str=str.substring(0, Math.min(str.length(), str.length()-1));
					htmlTable=htmlTable + "</table>";
					str=str+ "]";} 
		catch (IOException e) {e.printStackTrace();}	
		catch (JSONException e) {e.printStackTrace();}		
 		catch (ParserConfigurationException e) {e.printStackTrace();}
		catch (SAXException e) {e.printStackTrace();}}
}