package zak;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
@WebServlet("/MyServ")
public class MyServ extends HttpServlet {
	private static final long serialVersionUID = 1L;
    public MyServ() {super();}
    private String userName="";		private String password="";  		private String cadence;			private String release;   		private String homeUserName;	
    private String queryString; 	private String parentKeys;   		private String temp;   			private String defects;			private HttpSession userSession;
    private int totalTickets;		private int totalDefects;			private String epicDefects; 	private float testEffectiveness;
    private String storyList;		private String defectList; 			private String  releaseName="17.00.10";		private String cadenceName="16PROD07"; 
    String tblTypeCol;String tblTypeRow;String tblStatusCol;String tblStatusRow;String tblDetailCol; String tblDetailRow;String pieType; String pieStatus;
    
    CheckJiraLogin chk=new CheckJiraLogin();    GetKeys getKeys = new GetKeys();			getUS getUS=new getUS();
    GetDefects getDefects=new GetDefects();    	GetCadence getCadence=new GetCadence();
    
    public static void main(String[] args) throws UnsupportedEncodingException, ServletException, IOException {}	   
    
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException { 	
    	userSession=request.getSession();
    	switch (request.getParameter("pname")){  	
       	case "index": 
       		if(request.getParameter("txtUser").trim()==null || !request.getParameterMap().containsKey("txtPswd") || request.getParameter("txtUser").trim().equals("")){
       			homeUserName="Guest"; userName= "zakir.syed";  password= "*4085#Jan"; userSession.setAttribute("fqdUser", userName);  userSession.setAttribute("fqdPswd", password);
       			chk.checkJiraLogin(userName,password);}
    		else{
    			userName=request.getParameter("txtUser"); password=request.getParameter("txtPswd"); userSession.setAttribute("fqdUser", userName); userSession.setAttribute("fqdPswd", password);
    			chk.checkJiraLogin(userName,password); homeUserName=userName.replace("@TEAMFOCUSINS.COM", "").replace("@teamfocusins.com", "");
    			String[] userNameArray= homeUserName.split("\\.");
            	if (userNameArray.length>1){homeUserName=userNameArray[0].substring(0, 1).toUpperCase() + userNameArray[0].substring(1).toLowerCase() + " " + userNameArray[1].substring(0, 1).toUpperCase() + userNameArray[1].substring(1).toLowerCase();}             	}    		
    		queryString="cadence in(" + cadenceName + "," + cadenceName + "-1) and project!=SDLC and type in (deliverable, " + "\"" + "Production Defect" + "\"" + ") and summary!~" + "\"" + "general support" + "\"" ;
    		parentKeys=getKeys.getKeys(userName,password,queryString); cadence="0";
    		if(getKeys.ticketCtr>0) {cadence=String.valueOf((float) ((getKeys.done*100)/getKeys.ticketCtr));}
    		String cadencetableData= "[['" + cadenceName + "'," + getKeys.ticketCtr + "," + getKeys.done + "]]";
    		
    		queryString="fixVersion=" + releaseName + " and project=SDLC and type in (epic, " + "\"" + "Production Defect" + "\"" + ") and summary!~" + "\"" + "general support" + "\"" ;
    		parentKeys=getKeys.getKeys(userName,password,queryString); release="0";
    		if (getKeys.ticketCtr>0){release=String.valueOf((float) ((getKeys.done*100)/getKeys.ticketCtr));}
    		String releasetableData= "[['" + releaseName + "'," + getKeys.ticketCtr + "," + getKeys.done + "]]";
    		    		
    		parentKeys="Parent in (" + parentKeys + ") and type=Defect";     	
    		parentKeys=parentKeys.replace("(,", "(");	parentKeys=parentKeys.replace("done,", "");parentKeys=parentKeys.replace("indeterminate,", "");
    		getDefects.getDefects(userName,password,parentKeys); defects="0";
        	totalDefects=getDefects.don+getDefects.wad+getDefects.can+getDefects.wit+getDefects.rep+getDefects.unr+getDefects.dup;
    		if (totalDefects>0){
    			testEffectiveness=(float) (((totalDefects-getDefects.unr) *100)/(totalDefects));
    			defects=String.valueOf(Math.round(testEffectiveness));}
    		String defectstableData= "[['" + releaseName + "'," + totalDefects + "," + getDefects.unr + "]]";
        	String cadencecolData="['Cadence','Total Tickets','Closed']";String releasecolData="['Release','Total Tickets','Completed']";String defectscolData="['Release','Total Defects','Open']";	       	
    		HttpSession session=request.getSession(); session.setAttribute("cadence", cadence);	session.setAttribute("release", release); session.setAttribute("defect", defects);
    		session.setAttribute("cadencetabledata", cadencetableData); session.setAttribute("releasetabledata", releasetableData); session.setAttribute("defectstabledata", defectstableData);
    		session.setAttribute("cadencecoldata", cadencecolData);session.setAttribute("releasecoldata", releasecolData);session.setAttribute("defectscoldata",defectscolData);
    		session.setAttribute("userName", homeUserName); session.setAttribute("ticketCtr", chk.ticketCtr);
    		if(chk.flag){session.setAttribute("errMsg",""); response.sendRedirect("home.jsp?");}
    		else{
    			session.setAttribute("errMsg", "User Name & Password combination is invalid.<br /> Please correct and try again!");
    			session.setAttribute("userName",""); session.setAttribute("password",""); response.sendRedirect("index.jsp");    			
    		} break; 
    	case "cadence":
       		queryString=request.getParameter("txtQuery");
       		userName=userSession.getAttribute("fqdUser").toString();
       		password=userSession.getAttribute("fqdPswd").toString();
         	parentKeys=getKeys.getKeys(userName,password,queryString);
         	if(parentKeys.isEmpty()){         		
         		pieType="[['Type','Count'],['Prod Defects',0],['Epic',0]]";						pieStatus="[['Status','Count'],['Completed',0],['UAT',0],['Execution',0],['HL',0],['BA',0]]";
         		tblTypeCol="['Total Tickets','Production Defects','Epic']";     		   		tblTypeRow= "[['0',0,0]]";
         		tblStatusCol="['Total Tickets','Completed','UAT','Execution','HL','BA']";		tblStatusRow= "[['0',0,0]]";
         		tblDetailCol="['Summary','Status','Key']";						        		tblDetailRow= "[['0','0','0']]"; }
         	else{
         		totalTickets=getKeys.ep+getKeys.pd;
         		parentKeys="IssueKey in (" + parentKeys + ") and type in (Deliverable," + "\"" + "Production Defect" + "\"" + ") and project != SDLC";
         		parentKeys=parentKeys.replace("done,", "").replace("(,", "(");	
         		getCadence.getCadence(userName,password,parentKeys);
         		totalDefects=getCadence.don+getCadence.uat+getCadence.exec+getCadence.hl;
         		pieType="[['Type','Count'],['Prod Defects'," + getKeys.pd + "],['Epic'," + getKeys.ep + "]]";
         		pieStatus="[['Reason','Count'],['Completed'," + getCadence.don + "],['UAT'," + getCadence.uat + "],['Execution'," + getCadence.exec + "],['HL'," + getCadence.hl + "],['BA',"+ getCadence.ba + "]]";
         		tblTypeCol="['Total Tickets','Production Defects','Epic']";        		tblTypeRow= "[['" + totalTickets + "'," + getKeys.pd + "," + getKeys.ep + "]]";
         		tblStatusCol="['Total Tickets','Completed','UAT','Execution','HL','BA']";
         		tblStatusRow= "[['" + totalTickets + "'," + getCadence.don + "," + getCadence.uat + "," + getCadence.exec + "," + getCadence.hl + "," + getCadence.ba + "]]";
         		tblDetailCol="['Summary','Status','Key']";		tblDetailRow= getCadence.str; }
        	temp=request.getParameter("pname"); 
        	RequestDispatcher dispCadence = request.getRequestDispatcher(temp + ".jsp"); 
        	HttpSession session2=request.getSession(); 
    		session2.setAttribute("tblTypeCol", tblTypeCol); session2.setAttribute("tblTypeRow", tblTypeRow);
    		session2.setAttribute("tblStatusCol", tblStatusCol); session2.setAttribute("tblStatusRow", tblStatusRow);
    		session2.setAttribute("detailtablecol", tblDetailCol);session2.setAttribute("detailtablerow", tblDetailRow); 
    		session2.setAttribute("pieType", pieType); session2.setAttribute("pieStatus", pieStatus); request.setAttribute("cadenceTable", getCadence.htmlTable);
    		dispCadence.forward( request, response ); break;
    	case "kanban":
    		queryString=request.getParameter("txtQuery");
    		userName=userSession.getAttribute("fqdUser").toString();
       		password=userSession.getAttribute("fqdPswd").toString();
    		parentKeys=getKeys.getKeys(userName,password,queryString);
    		epicDefects="blank"; storyList=""; defectList="";
    		if(!parentKeys.isEmpty() && !request.getParameter("release").equalsIgnoreCase("--Select Release--")){
    	 		parentKeys=parentKeys.replace("indeterminate,", "").replace("done,", "");
         		epicDefects="<select name=" + "\"" + "comboUS" + "\""+ " id=" + "\"" + "comboUS" + "\"" + " onchange=" + "\"" + "fillBox()" + "\"" + "><option>--Select Epic--" + parentKeys.replace(",new", "").replace(",", "</option><option>") + "</option></select>";
         		if(request.getParameterMap().containsKey("comboUS")){
         			if(!request.getParameter("comboUS").equalsIgnoreCase("--Select Epic--") && request.getParameter("comboUS")!=null){
         				parentKeys="\"" + "Epic Link" + "\"" + "=" +  request.getParameter("comboUS") + " and type=Story";
         				getUS.getUserStories(userName,password,parentKeys); 	storyList=getUS.htmlTable;
         				parentKeys="parent=" +  request.getParameter("comboUS") + " and type=defect";
         				getUS.getUserStories(userName,password,parentKeys);		defectList=getUS.htmlTable;
         				defectList=defectList.replace("<table id=" + "\"" + "userStory", "<table id=" + "\"" + "defectList");}}}    		
    		temp=request.getParameter("pname"); 
        	RequestDispatcher dispKanban = request.getRequestDispatcher(temp + ".jsp");
        	HttpSession session3=request.getSession(); 
    		if(epicDefects=="blank"){epicDefects="<select name=" + "\"" + "comboUS" + "\"" + " onchange=" + "\"" + "fillBox()" + "\"" + " id=" + "\"" + "comboUS" + "\"" + "><option>--Select Epic--</option></select>";}
    		request.setAttribute("storyList", storyList);request.setAttribute("defectList", defectList);
    		session3.setAttribute("epicDefects", epicDefects); 
        	dispKanban.forward( request, response );break;
    	default:
    		queryString=request.getParameter("txtQuery");userName=userSession.getAttribute("fqdUser").toString();password=userSession.getAttribute("fqdPswd").toString();
    		parentKeys=getKeys.getKeys(userName,password,queryString);
    		if(parentKeys.isEmpty()){ 	
    			pieType="[['Type','Count'],['Prod Defects',0],['Epic',0]]";			pieStatus="[['Reason','Count'],['Unresolved',0],['Fixed',0],['Works as Designed',0],['Withdrawn',0],['Duplicate',0],['Cannot Reproduce',0]]";
    			tblTypeCol="['Total Tickets','Production Defects','Epic']"; 		tblTypeRow= "[['0',0,0]]";
    			tblStatusRow= "[['0',0,0,0,0,0,0,0]]";								tblStatusCol="['Total Defects','Unresolved','Fixed','Works As Designed','Withrawn','Duplicate','Cannot Reproduce','Test Effectiveness']";    	
    			tblDetailCol="['Total Defects','Resolved as Done','Open']";	   		tblDetailRow= "[['0','0','0']]";}
         	else{
         		parentKeys=parentKeys.replace("done,", "");
         		totalTickets= getKeys.ep+getKeys.pd;
         		tblTypeCol="['Total Tickets','Production Defects','Epic']";         tblTypeRow= "[['" + totalTickets + "'," + getKeys.pd + "," + getKeys.ep + "]]";
         		pieType="[['Type','Count'],['Prod Defects'," + getKeys.pd + "],['Epic'," + getKeys.ep + "]]";
         		parentKeys="Parent in (" + parentKeys + ") and type=Defect and Status!=Open";
    			parentKeys=parentKeys.replace("(,", "(");	
         		getDefects.getDefects(userName,password,parentKeys);
         		totalDefects=getDefects.don+getDefects.wad+getDefects.can+getDefects.wit+getDefects.rep+getDefects.unr+getDefects.dup;
         		if(totalDefects>0){
         			testEffectiveness=(float) ((getDefects.don) *100)/(getDefects.don + getDefects.wad +  getDefects.rep + getDefects.dup);
         			tblStatusCol="['Total Defects','Unresolved','Fixed','Works As Designed','Withrawn','Duplicate','Cannot Reproduce','Test Effectiveness']";    	
         	   		tblStatusRow= "[['" + totalDefects + "'," + getDefects.unr + "," + getDefects.don + "," + getDefects.wad + "," + getDefects.wit + "," + getDefects.dup + "," + getDefects.can + "," + testEffectiveness + "]]";
         	   		pieStatus="[['Reason','Count'],['Unresolved'," + getDefects.unr + "],['Fixed'," + getDefects.don + "],['Works as Designed'," + getDefects.wad + "],['Withdrawn'," + getDefects.wit + "],['Duplicate',"+ getDefects.dup + "],['Cannot Reproduce'," + getDefects.can + "]]";
         	   	tblDetailCol="['Total Defects','Resolved as Done','Open']";			tblDetailRow= "[['" + totalDefects + "'," + getDefects.don + "," + getDefects.unr + "]]";	}	
         		else{
         			tblStatusRow= "[['0',0,0,0,0,0,0,0]]";			tblStatusCol="['Total Defects','Unresolved','Fixed','Works As Designed','Withrawn','Duplicate','Cannot Reproduce','Test Effectiveness']";    		
         			tblDetailCol="['Total Defects','Resolved as Done','Open']";		pieStatus="[['Reason','Count'],['Unresolved',0],['Fixed',0],['Works as Designed',0],['Withdrawn',0],['Duplicate',0],['Cannot Reproduce',0]]";
             		tblDetailRow= "[['0','0','0']]";}}         	
        	temp=request.getParameter("pname"); 
        	RequestDispatcher dispDefault = request.getRequestDispatcher(temp + ".jsp");
        	request.setAttribute("Name", getDefects.str); 
        	HttpSession session4=request.getSession(); 
    		session4.setAttribute("tblTypeCol", tblTypeCol); session4.setAttribute("tblTypeRow", tblTypeRow);
    		session4.setAttribute("tblStatusCol", tblStatusCol); session4.setAttribute("tblStatusRow", tblStatusRow);
    		session4.setAttribute("tblDetailCol", tblDetailCol);session4.setAttribute("tblDetailRow", tblDetailRow); 
    		session4.setAttribute("pieType", pieType); session4.setAttribute("pieStatus", pieStatus);
    		request.setAttribute("epicTable", getKeys.htmlTable);request.setAttribute("defectTable", getDefects.htmlTable);
        	dispDefault.forward( request, response );break;}  		
	}     
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {}
}