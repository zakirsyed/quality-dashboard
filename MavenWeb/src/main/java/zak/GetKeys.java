package zak;

import java.io.IOException;
import java.io.StringReader;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.XML;
import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.core.util.Base64;

public class GetKeys {
	public int done; 			public int pending;			public boolean hLink;    	public boolean issueType; 	public String tempStr;
	public boolean status;		private String parentKeys;	public int pd;            	public int ep;				public int ticketCtr;   	
	public String htmlTable;	public String elementName;
	public String getKeys(String userName,String password, String queryString){
        try {
        		htmlTable="<table id=" + "\"" + "epicProdDefects" + "\"" + "class=" + "\"" + "display compact" + "\"" + " cellspacing=" + "\"" + "0" + "\"" + " width=" + "\"" + "90%" + "\"" + "><thead><tr><th>Summary</th><th>Type</th><th>Client</th><th>Team</th><th>Status</th><th>Key</th></tr></thead>";            
        		ep=0; pd=0;done=0; pending=0; parentKeys=""; ticketCtr=0;
        		Client client=Client.create();
       	 		String temp=" and " + "\"" + "Execution Team" + "\"" + "=" + "\"" + "--Select Team--" + "\"";
       	 		queryString=queryString.replace(temp, "");
       	 		String auth = new String(Base64.encode(userName + ":" + password));
       	 		queryString=queryString.replace("=","%3D");			queryString=queryString.replace(" ","%20");
       	 		queryString=queryString.replace("\"","%22");		queryString=queryString.replace("!~","%20!~");	
       	 		queryString=queryString.replace("new", "").replace("done", "").replace("indeterminate", "");	
       	 		queryString=queryString + "&fields=key,issuetype,status,summary,customfield_12011,customfield_12012,&maxResults=10000";
       	 		//System.out.println(queryString);
       	 		WebResource resource=client.resource("https://focustech.atlassian.net/rest/api/2/search?jql=" + queryString);
       	 		ClientResponse resp=resource.header("Authorization", "Basic "+ auth).type("application/json").accept("application/json").get(ClientResponse.class);
				JSONObject json=new JSONObject(resp.getEntity(String.class));
				String xml=XML.toString(json);
				//System.out.println(xml);
				
				xml="<root>" + xml + "</root>";
				xml=xml.replace("<48x48>",""); 	xml=xml.replace("</48x48>","");		xml=xml.replace("<24x24>","");		xml=xml.replace("</24x24>","");
				xml=xml.replace("<32x32>","");	xml=xml.replace("</32x32>","");		xml=xml.replace("<16x16>","");		xml=xml.replace("</16x16>","");
				
				tempStr=xml.substring(0, Math.min(xml.length(), 51));
				tempStr=tempStr.replace("<root><expand>schema,names</expand><total>", ""); 
				String[] xmlSplit = tempStr.split("<");

				if (xmlSplit.length >0 && xmlSplit[0].isEmpty()==false) {
					ticketCtr=Integer.parseInt(xmlSplit[0]);			
					SAXParser p=SAXParserFactory.newInstance().newSAXParser();
					DefaultHandler handler=new DefaultHandler(){
					public void startDocument() throws SAXException{parentKeys="";}
					public void startElement(String uri, String localName,String qName,Attributes attributes) throws SAXException{elementName=qName.trim();}
					public void characters(char ch[], int start, int length) throws SAXException{
						String tempStr=new String(ch,start,length).trim();			
						tempStr= tempStr.substring(0, Math.min(tempStr.length(), 70));
						if (elementName.equalsIgnoreCase("key")){if(!tempStr.trim().equalsIgnoreCase("done") && !tempStr.trim().equalsIgnoreCase("indeterminate")){parentKeys = parentKeys+ "," + tempStr ; tempStr="<a href="+ "\"" + "https://focustech.atlassian.net/browse/" + tempStr + "\"" + "target=" + "\"" + "blank" + "\"" + ">" + tempStr + "</a>"; htmlTable=htmlTable+"<td>" + tempStr.trim() + "</td></tr>";}}
						if (elementName.equalsIgnoreCase("summary")){htmlTable=htmlTable+"<tr><td>" + tempStr.trim() + "</td>";}
						if (elementName.equalsIgnoreCase("value")){htmlTable=htmlTable+"<td>" + tempStr.trim() + "</td>";}
						if (elementName.equalsIgnoreCase("name")){
							switch(tempStr){
								case "Closed": done=done+1; htmlTable=htmlTable+"<td>" + tempStr.trim() + "</td>"; break;					
								case "Prod": done=done+1; htmlTable=htmlTable+"<td>" + tempStr.trim() + "</td>"; break;
								case "UAT Done": done=done+1; htmlTable=htmlTable+"<td>" + tempStr.trim() + "</td>";  break;				
								case "UAT In Progress" : pending=pending+1; htmlTable=htmlTable+"<td>" + tempStr.trim() + "</td>"; break;
								case "UAT Deployed" : pending=pending+1; htmlTable=htmlTable+"<td>" + tempStr.trim() + "</td>"; break;
								case "QA Done" : pending=pending+1; htmlTable=htmlTable+"<td>" + tempStr.trim() + "</td>"; break;			
								case "QA In Progress" : pending=pending+1; htmlTable=htmlTable+"<td>" + tempStr.trim() + "</td>"; break;
								case "Staging" : pending=pending+1; htmlTable=htmlTable+"<td>" + tempStr.trim() + "</td>"; break;			
								case "Dev Done" : pending=pending+1; htmlTable=htmlTable+"<td>" + tempStr.trim() + "</td>"; break;
								case "Dev In Progress" : pending=pending+1; htmlTable=htmlTable+"<td>" + tempStr.trim() + "</td>"; break;	
								case "BA Done" : pending=pending+1; htmlTable=htmlTable+"<td>" + tempStr.trim() + "</td>"; break;
								case "BA" : pending=pending+1; htmlTable=htmlTable+"<td>" + tempStr.trim() + "</td>"; break;				
								case "Production Defect": pd=pd+1; htmlTable=htmlTable+"<td>" + tempStr.trim() + "</td>"; break;
								case "Epic": ep=ep+1; htmlTable=htmlTable+"<td>" + tempStr.trim() + "</td>"; break;
								case "Deliverable": ep=ep+1; htmlTable=htmlTable+"<td>" + tempStr.trim() + "</td>"; break;
								default: pending=pending+1; break;}}}
					public void endElement(String uri, String localName,String qName) throws SAXException{}
					public void endDocument() throws SAXException{htmlTable=htmlTable + "</table>";}};	
				p.parse(new InputSource(new StringReader(xml)),handler);} }
		catch (IOException e) {e.printStackTrace();}	
		catch (JSONException e) {e.printStackTrace(); }		
 		catch (ParserConfigurationException e) {e.printStackTrace();}
		catch (SAXException e) {e.printStackTrace();}
        return parentKeys;}
}