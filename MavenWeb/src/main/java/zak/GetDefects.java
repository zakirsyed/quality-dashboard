package zak;
import java.io.IOException;
import java.io.StringReader;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.XML;
import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.core.util.Base64;

public class GetDefects {
	public String str;		public String elementName; 		public int don;    	public int wad;    		public String htmlTable;
	public int can;			public int unr;					public int wit;     public int rep;			public int dup;
	
	public void getDefects(String userName,String password,String parentKeys){
		don=0; wad=0; can=0; wit=0; rep=0; unr=0; dup=0;str="[";
        try {	
        		htmlTable="<table id=" + "\"" + "Defects" + "\"" + "class=" + "\"" + "display compact" + "\"" + " cellspacing=" + "\"" + "0" + "\"" + " width=" + "\"" + "90%" + "\"" + "><thead><tr><th>Summary</th><th>Resolution</th><th>Key</th></tr></thead>";            
        		Client client=Client.create();
				String auth = new String(Base64.encode(userName + ":" + password));
				parentKeys=parentKeys.replace("=","%3D");		parentKeys=parentKeys.replace(" ","%20");			parentKeys=parentKeys.replace("\"","%22");
				parentKeys=parentKeys.replace("done,", "");		parentKeys=parentKeys.replace("indeterminate,", "");	parentKeys=parentKeys.replace("new,", "");
				parentKeys=parentKeys + "&fields=key,resolution,summary&maxResults=10000";
				//System.out.println(parentKeys);
				WebResource resource=client.resource("https://focustech.atlassian.net/rest/api/2/search?jql=" + parentKeys);
				ClientResponse resp=resource.header("Authorization", "Basic "+ auth).type("application/json").accept("application/json").get(ClientResponse.class);
				JSONObject json=new JSONObject(resp.getEntity(String.class));
				String xml=XML.toString(json);
				//System.out.println(xml);
				xml="<root>" + xml + "</root>";	xml=xml.replace("&", "");
				xml=xml.replace("<48x48>","");	xml=xml.replace("</48x48>","");	xml=xml.replace("<24x24>","");	xml=xml.replace("</24x24>","");
				xml=xml.replace("<32x32>","");	xml=xml.replace("</32x32>","");	xml=xml.replace("<16x16>","");	xml=xml.replace("</16x16>","");
				xml = xml.replaceAll("\\[", "").replaceAll("\\]","");	
				SAXParser p=SAXParserFactory.newInstance().newSAXParser();
				DefaultHandler handler=new DefaultHandler(){
					public void startDocument() throws SAXException{}
					public void startElement(String uri, String localName,String qName,Attributes attributes) throws SAXException{elementName=qName.trim();}
					public void characters(char ch[], int start, int length) throws SAXException{
						String tempStr=new String(ch,start,length).trim();
							
						tempStr = tempStr.replace("(", "").replace(")", "");
						tempStr=tempStr.replace("\"", "");		tempStr=tempStr.replace("'", "");
						tempStr=tempStr.replace("*", "");		tempStr=tempStr.replace("=", "");		tempStr=tempStr.replace("#", "");
						if (tempStr.length()>0){tempStr= tempStr.substring(0, Math.min(tempStr.length(), 80));}
						if(elementName.equalsIgnoreCase("resolution")){str=str + "\'" +  "Unresolved" + "',";unr=unr+1;htmlTable=htmlTable+"<td>" + tempStr.trim() + "</td>";}
						if (elementName.equalsIgnoreCase("key")){str=str + "\'" + tempStr + "'],";tempStr="<a href="+ "\"" + "https://focustech.atlassian.net/browse/" + tempStr + "\"" + "target=" + "\"" + "blank" + "\"" + ">" + tempStr + "</a>";htmlTable=htmlTable+"<td>" + tempStr.trim() + "</td></tr>";}
						if (elementName.equalsIgnoreCase("summary")){str=str + "['" + tempStr.trim() + "',";htmlTable=htmlTable+"<tr><td>" + tempStr.trim() + "</td>";}
						if (elementName.equalsIgnoreCase("name")){
							str=str + "\'" +  tempStr + "',";  htmlTable=htmlTable+"<td>" + tempStr.trim() + "</td>";
							switch (tempStr){
								case "Done": don=don+1; break;			case "Canceled": can=can+1; break;			
								case "Withdrawn": wit=wit+1; break;		case "Works as Designed": wad=wad+1; break;
								case "Duplicate": dup=dup+1; break;		case "Cannot Reproduce": rep=rep+1; break;}}
						}
					public void endElement(String uri, String localName,String qName) throws SAXException{}
					public void endDocument() throws SAXException{
						if (str.length()>0){str=str.substring(0, Math.min(str.length(), str.length()-1)); str=str + "]";}
						htmlTable=htmlTable + "</table>";
						}};
				p.parse(new InputSource(new StringReader(xml)),handler);} 
		catch (IOException e) {e.printStackTrace();}	
		catch (JSONException e) {e.printStackTrace();}		
 		catch (ParserConfigurationException e) {e.printStackTrace();}
		catch (SAXException e) {e.printStackTrace();}}
}